package com.company;

public class Main {

    public static void main(String[] args) {
	     Shape circle = new ShapeBuilder("Circle").selectArea(1).selectName("circle").build();
         Shape rectangle = new ShapeBuilder("Rectangle").selectArea(3).selectName("rectangle").build();
         Shape triangle = new ShapeBuilder("Triangle").selectArea(4).selectName("triangle").build();

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(triangle);
    }
}
