package com.company;

public abstract class Shape {
    public int area;
    public String name;

    public Shape() {
    }

    public Shape(int area, String name) {
        this.area = area;
        this.name = name;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public abstract double getArea();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString(){
        return getName();
    }
}