package com.company;

public class Rectangle extends Shape{
    private double a;
    private double b;

    public Rectangle() {
    }

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Rectangle(int area,String name, double a, double b) {
        super(area, name);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public String toString() {
        return super.toString() + " with sides " +
                "a=" + a + ", b=" + b;
    }
}
