package com.company;

public class ShapeBuilder {
    private Shape shape;
    private int area;
    private String name;

    public ShapeBuilder(String shapeType) {
        switch (shapeType.toLowerCase()) {
            case ("circle"):
                shape = new Circle();
                break;
            case ("rectangle"):
                shape = new Rectangle();
                break;
            case ("triangle"):
                shape = new Triangle();
                break;
            default:
                break;
        }

    }

    public ShapeBuilder selectName(String name) {
//        shape.name = name;
        shape.setName(name);
        return this;


    }

    public ShapeBuilder selectArea(int area) {
//        shape.area = area;
        shape.setArea(area);
        return this;
    }

    public Shape build() {
        return shape;
    }
}
